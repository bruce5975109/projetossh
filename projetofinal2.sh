#!/bin/bash

# Verifica se o pacote 'openssh-server' está instalado
check_ssh_server() {
  dpkg -s openssh-server &> /dev/null
  return $?
}

# Instala o pacote 'openssh-server'
install_ssh_server() {
  sudo apt-get update
  sudo apt-get install -y openssh-server
}

# Função para exibir uma caixa de diálogo para selecionar o arquivo de origem
select_source_file() {
  source_file=$(zenity --file-selection --title="Selecione o arquivo de origem")
  if [[ -z "$source_file" ]]; then
    zenity --error --text="Nenhum arquivo de origem selecionado."
    exit 1
  fi
}

# Função para exibir uma caixa de diálogo para selecionar o destino
select_destination() {
  destination=$(zenity --entry --title="Selecione o destino" --text="Digite o destino (usuário@host:/caminho):")
  if [[ -z "$destination" ]]; then
    zenity --error --text="Nenhum destino selecionado."
    exit 1
  fi
}

# Função para exibir uma caixa de diálogo para inserir o endereço do servidor SSH
enter_ssh_address() {
  ssh_address=$(zenity --entry --title="Servidor SSH" --text="Digite o endereço do servidor SSH:")
  if [[ -z "$ssh_address" ]]; then
    zenity --error --text="Nenhum endereço de servidor SSH fornecido."
    exit 1
  fi
}

# Função para copiar o arquivo selecionado para o destino
copy_file() {
  local source="$1"
  local destination="$2"

  # Copiar o arquivo usando o comando scp
  scp "$source" "$destination"

  if [[ $? -eq 0 ]]; then
    zenity --info --text="Cópia de arquivo bem-sucedida."
  else
    zenity --error --text="Falha na cópia do arquivo."
  fi
}

# Função para verificar a integridade do arquivo usando MD5
verify_md5() {
  local file="$1"
  local md5_source=$(md5sum "$file" | awk '{print $1}')
  local md5_destination=$(ssh "$ssh_address" "md5sum $destination" | awk '{print $1}')

  if [[ "$md5_source" == "$md5_destination" ]]; then
    zenity --info --text="Verificação MD5 bem-sucedida. O arquivo foi copiado corretamente."
  else
    zenity --error --text="Verificação MD5 falhou. O arquivo pode ter sido corrompido durante a cópia."
  fi
}

# Função para exibir a lista de arquivos no cliente ou no servidor
show_file_list() {
  local ssh_address="$1"

  # Listar arquivos no cliente
  local local_files=$(ls)

  # Listar arquivos no servidor
  local remote_files=$(ssh "$ssh_address" ls)

  # Exibir a lista de arquivos em uma caixa de diálogo
  zenity --info --title="Lista de Arquivos" --text="Arquivos locais:
$local_files

Arquivos remotos ($ssh_address):
$remote_files"
}

# Função principal
main() {
  if ! check_ssh_server; then
    zenity --question --text="O servidor SSH não está instalado. Deseja instalá-lo agora?"
    if [[ $? -eq 0 ]]; then
      install_ssh_server
    else
      exit 1
    fi
  fi

  # Exibir menu inicial
  action=$(zenity --list --title="Menu" --column="Ação" \
    "Copiar arquivo do cliente para o servidor" \
    "Copiar arquivo do servidor para o cliente" \
    "Visualizar lista de arquivos no cliente e no servidor")

  case "$action" in
    "Copiar arquivo do cliente para o servidor")
      select_source_file
      enter_ssh_address
      select_destination
      copy_file "$source_file" "$destination"
      verify_md5 "$source_file"
      ;;
    "Copiar arquivo do servidor para o cliente")
      enter_ssh_address
      select_source_file
      copy_file "$ssh_address:$source_file" "."
      verify_md5 "$source_file"
      ;;
    "Visualizar lista de arquivos no cliente e no servidor")
      enter_ssh_address
      show_file_list "$ssh_address"
      ;;
    *)
      zenity --error --text="Ação inválida selecionada."
      exit 1
      ;;
  esac
}

# Executar a função principal
main
